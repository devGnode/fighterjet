
export class Collider{

    static ALL_COLL   = 0x00;
    static NORTH_COLL = 0x01;
    static SOUTH_COLL = 0x02;
    static EAST_COLL  = 0x04;
    static WEST_COLL  = 0x08;

    static NORTH_OR         = 0x01;
    static NORTH_EAST_OR    = 0x02;
    static EAST_OR          = 0x03;
    static SOUTH_EAST_OR    = 0x04;
    static SOUTH_OR         = 0x05;
    static SOUTH_WEST_OR    = 0x06;
    static WEST_OR          = 0x07;
    static NORTH_WEST_OR    = 0x08;
    /****
     * @hasCollision : 2D
     // X -> 0 & 3
     // X -> 2 & 1
     // Y -> 0 & 3
     // Y -> 1 & 2
     * @param objectA
     * @param objectB
     * @param withCheckingColoration
     * @returns {boolean|boolean}
     */
    static hasCollision(  objectA, objectB, withCheckingColoration = false  ){
        let v = objectA.getVertexArea(false ), m =  objectB.getVertexArea(false );
        return (v[0].x-m[3].x < 0 && m[2].x-v[1].x < 0 ) && (v[0].y-m[3].y < 0&&m[1].y-v[2].y < 0)/* && ( !withCheckingColoration || (objectA.getPixel(v[0].x,v[0].y) !== 0 || objectB.getPixel(v[0].x,v[0].y) !== 0 ))*/;
    }

    static collisionOrientation(  objectA, objectB ){
        let v = objectA.getVertexArea(true ), m =  objectB.getVertexArea(true );

    }

    static hasCollisionOrientationTo(  objectA, objectB, orientation = Collider.NORTH_OR ){
        let v = objectA.getVertexArea(true ), m =  objectB.getVertexArea(true );

    }

    static orientation( objectA, objectB ){
        let v = objectA.getVertexArea(false ),
            m =  objectB.getVertexArea(false ),
            dNW =0, dN =0, dNE =0, dE = 0,
            dSE = 0, dS =0, dSW = 0, dW =0;

        let c = objectB.getDirection() - Collider.NORTH_OR;
        dN = (Collider.NORTH_OR +c);
        dN -= dN > 8 ?8 :0;
        dNE = (Collider.NORTH_EAST_OR + c) ;
        dNE -= dNE > 8 ?8 :0;
        dE = (Collider.EAST_OR + c);
        dE -= dE > 8 ?8 :0;
        dSE = (Collider.SOUTH_EAST_OR + c);
        dSE -= dSE > 8 ?8 :0;
        dS = (Collider.SOUTH_OR + c);
        dS -= dS > 8?8 :0;
        dSW = (Collider.SOUTH_WEST_OR + c);
        dSW -= dSW >= 8 ?8 :0;
        dW = (Collider.WEST_OR + c);
        dW -= dW > 8 ?8 :0;
        dNW = (Collider.NORTH_WEST_OR + c);
        dNW -= dNW > 8 ?8 :0;

        if( v[0].x-m[3].x >= 0 && m[2].x-v[1].x < 0 && v[0].y-m[3].y < 0 && m[1].y-v[2].y >= 0) return dNE;
        if( v[0].x-m[3].x >= 0 && m[2].x-v[1].x < 0 && v[0].y-m[3].y < 0 && m[1].y-v[2].y < 0) return dE;
        if(( v[0].x-m[3].x >= 0 && m[2].x-v[1].x < 0 && v[0].y-m[3].y >= 0 && m[1].y-v[2].y < 0))return dSE;

        if( v[0].x-m[3].x < 0 && m[2].x-v[1].x < 0 && v[0].y-m[3].y < 0 && m[1].y-v[2].y >= 0) return dN;
        if( v[0].x-m[3].x < 0 && m[2].x-v[1].x < 0 && v[0].y-m[3].y >= 0 && m[1].y-v[2].y < 0) return dS;

         if( v[0].x-m[3].x < 0 && m[2].x-v[1].x >= 0 && v[0].y-m[3].y < 0 && m[1].y-v[2].y >= 0) return dNW;
         if( v[0].x-m[3].x < 0 && m[2].x-v[1].x >= 0 && v[0].y-m[3].y < 0 && m[1].y-v[2].y < 0) return dW;
         if( v[0].x-m[3].x < 0 && m[2].x-v[1].x >= 0 && v[0].y-m[3].y >= 0 && m[1].y-v[2].y < 0) return dSW;

         return Collider.ALL_COLL;
    }

}

// dir north
//      north
//west          est
//      sud

// dir est
//      West
//south     north
//      est

// dir south
//      sud
// est          west
//      north

// dir west
//      est
// north       south
//      west


//    1  2   3
//  7           4
//    7  6   5

//  6-2 = 4

//  1+2 = 3
//  2+2 = 4
//  3+2 = 1
//  4+2 = 2


//    1
// 4    2
//   3

//  n  - 1 = 0
//
// 1 - 0 = 1
// 2 - 0 = 2
// 3 - 0 = 3
// 4 - 0 = 4

// 3 - 1 = 2

// 1 + 2 = 3
// 2 + 2 = 4
// 3 + 2 = 5 => 5 - 4 = 1
// 4 + 2 = 6 => 6 - 4 = 2