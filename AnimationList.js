
/***
 * @AnimationList : for 2d map animation
 */
export class AnimationList{

    static STATIC_FRAME = 0x00;
    static DYNAMIC_FRAME= 0x01;

    #staticFrame        = [];
    #dynamicElements    = [];
    #thread;
    #frame;

    constructor(thread = null) {
        this.#thread = thread;
    }
    /**
     * @return {AnimationList}
     */
    raz(){
        this.#staticFrame=[];
        this.#dynamicElements=[];
        return this;
    }
    /***
     * @param animateObject
     * @param type
     * @return {AnimationList}
     */
    set( animateObject = null, type = AnimationList.DYNAMIC_FRAME ){
        if( type === AnimationList.DYNAMIC_FRAME )
        this.#dynamicElements.push(animateObject);
        else{
            this.#staticFrame.push(animateObject);
        }
        return this;
    }
    /****
     */
    setDynamicalFrame( animateObject = null ){
        this.#dynamicElements.push(animateObject);
    }
    /****
     */
    setStaticFrame( animateObject = null ){
        this.#staticFrame.push(animateObject);
    }
    /***
     */
    getFrameBuffer(){
        return FrameBuffer.builder( this.#frame.copy(), {
            x: this.#frame.getWidth(),
            y: this.#frame.getHeight()
        });
    }

    staticRender( ){

    }

    render( ){

    }
}