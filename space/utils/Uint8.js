class Uint8 extends Uint8Array{

    constructor( data = null ) {
        super(data);
    }

    has(value){return super.indexOf(value)>-1;}

}
export default Uint8;