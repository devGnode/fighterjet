class Utils {

    /***
     * random lucky chance
     * to unlock animation
     * @param xLucky
     * @return {boolean}
     */
    static getRandomLucky( xLucky = 2 ){
        return Math.round(Math.random() * xLucky) === Math.round(Math.random() * xLucky);
    }

}
export default Utils;