import Utils    from "../utils/Utils.js";
import Bot      from "../pojo/players/Bot.js";
import Player   from "../pojo/players/Player.js";

/***
 * Weapons
 */
import Browning from "../pojo/weapons/Browning.js";
import M230     from "../pojo/weapons/M230.js";
import Nuclear  from "../pojo/weapons/Nuclear.js";
import TommyGun from "../pojo/weapons/TommyGun.js";
import WarHead  from "../pojo/weapons/WarHead.js";

class Factory {
f
    /***
     * @param magazine
     * @param name
     * @return {WarHead|Browning|TommyGun|Nuclear|M230}
     */
    static weapon( magazine = 0x00, name = null ){
        switch (name) {
            case "M230":     return new M230(magazine||M230.MAG_MAX);
            case "Browning": return new Browning(magazine||Browning.MAG_MAX);
            case "Nuclear":  return  new Nuclear(magazine||Nuclear.MAG_MAX);
            case "WarHead":  return new WarHead(magazine||WarHead.MAG_MAX);
            case "TommyGun":
            default: return new TommyGun(magazine||TommyGun.MAC_MAX);
        }
    }

    /***
     * @param name
     * @return {WarHead|Browning|TommyGun|Nuclear|M230}
     */
    static infiniteWeapon( name = null ){return Factory.weapon(Number.POSITIVE_INFINITY,name); }

    /***
     * @param tileNo
     * @param x
     * @param y
     * @param speed
     * @param weaponRate
     * @param name
     * @param weapons
     * @return {Bot}
     */
    static bot( tileNo = 0x00, x = 0x00, y = 0x00, speed = 0x01, weaponRate = 0x01, weapons = [] ){
        let bot = new Bot( tileNo, "bot", x, y, speed, weaponRate);
        weapons.forEach(weaponName=>{bot.setWeapon(Factory.infiniteWeapon(weaponName))});
        return bot;
    }

    /***
     * @param tileNo
     * @param x
     * @param y
     * @param speed
     * @param weapons
     * @param name
     * @return {Player}
     */
    static player( tileNo = 0x00, x = 0x00, y = 0x00, speed = 0x00, weapons = [] , name = "player1" ){
        let player = new Player( tileNo, name, x, y, speed);

        weapons.forEach(weaponName=>{player.setWeapon(Factory.weapon(null, weaponName))});
        if(Utils.getRandomLucky(1000000 )) player.setWeapon(Factory.infiniteWeapon("Nuclear"));

        return player;
    }

    static MAGAZINE_GIFT = 0x00;

    static gift( giftType = Factory.MAGAZINE_GIFT  ){

    }

}
export default Factory;