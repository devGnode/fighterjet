import Bullet     from "./Bullet.js";
import FighterJet from "./FighterJet.js";
import Player     from "./players/Player.js";

class NuclearWarHead extends Bullet{

    constructor( damage = 0.00 ) {
        super(-1,damage,Number.POSITIVE_INFINITY,Number.POSITIVE_INFINITY,0x10,-1);
    }

    /***
     * @param animation
     * @param thread
     * @param frame
     * @param elements
     */
    perform( animation = null, thread = null, frame = null, elements = null){

       // if( thread.getTick%250===1)
           elements.forEach(object=> ( object instanceof FighterJet && !(object instanceof Player) ) ? object.setDamage(this) : void 0);
           this.setDestroyFrame();

           return this.destroyFrame;
    }

}
export default NuclearWarHead;