import AbsAnimatedObject from "../AbsAnimateObject.js";
import Textures          from "../../dat/Textures.js";

class TestTile extends AbsAnimatedObject{

    constructor( tile = 0x00, x = 150, y = 150) {
        super(
            null,
            x,
            y,
            0x00
        );
        this.setSkin(tile);
    }

    setSkin( skinNo = 0x00 ){
        if(Textures[skinNo&=0xff]) return  this.setFrameBuffer(FrameBuffer.builder( Textures[skinNo].dat,{ x: Textures[skinNo].width, y: Textures[skinNo].height }));
        return null;
    }

    perform( ){return this.destroyFrame;}

}

export default TestTile;