import Render from "../lib/Render.js";

/***
 *
 */
class EventsPerforming {

    /**
     * Array of AbsAnimateObject object
     * @type {[]}
     */
    #elements;
    #thread;
    #frame;

    constructor(thread = null, monitor = null ) {
        this.#thread = thread;
        this.#frame  = Render.builder().resize(400,650).resetScreen(0x000000 );
        this.raz();
    }

    /***
     * @return {*[]}
     */
    getElements( ){return this.#elements;}

    /**
     * @return {EventsPerforming}
     */
    raz(){ this.#elements=[]; return this;}

    /***
     * @param animateObject
     * @return {EventsPerforming}
     */
    set( animateObject = null ){this.#elements.push(animateObject); return this;}

    /***
     * @param width
     * @param height
     * @param color
     * @return {EventsPerforming}
     */
    resize( width = 2, height = 2, color = 0x000000 ){
        this.#frame = Render.builder().resize(width,height).resetScreen(color);
        return this;
    }

    /***
     * @return {Render}
     */
    getFrameBuffer(){
        return Render.builder( this.#frame.copy(), {
            x: this.#frame.getWidth(),
            y: this.#frame.getHeight()
        } );
    }

    /***
     * @param frameBuffer
     * @return {{setTiles: function(*, *, *, *, *=): (boolean|*), setTilesByOffset: function(*, *=, *=, *=): *}|Tiles|{setTiles: setTiles, setTilesByOffset: (function(*, *=, *=, *=): boolean)}}
     */
    tilesOptions( frameBuffer = null ){
        return frameBuffer
            .tiles()
            .filter((c,color)=>color>0)
            .setCenter(true);
    }

    /***
     * @return {Render}
     */
    perform( ){
        let tmp = [],validated,
            frame = this.getFrameBuffer(),
            tiles = this.tilesOptions(frame);

        if(this.#elements.length>0){
            validated = this.#elements.filter(v=>{
                let performing = !v.perform(tmp,this.#thread,frame, [].concat(this.#elements)),
                bf;
                if(performing&&(bf = v.getFrameBuffer())){
                    // drawing on the framebuffer
                    if(v.displayFrame)
                    tiles.setOffsetX(bf.getWidth())
                         .setOffsetY(bf.getHeight())
                         .setTile(v.X,v.Y,bf.getUin32FrameBuffer());
                }
                return performing;
            });
            this.#elements = tmp.concat(validated);
        }

        return frame;
    }

}
/***
 */
export default EventsPerforming;