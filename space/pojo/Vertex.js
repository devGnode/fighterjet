/**
 *  2d vertex
 * */
class Vertex{

    #x;
    #y;

    constructor( x = null, y = null) {
        this.#x = x;
        this.#y = y;
    }

    get x(){return this.#x; }
    get y(){return this.#y; }

    set y( value  ){ this.#y = value;}

    setX( x = 0 ){
        this.#x = x;
        return this;
    }

    setY( y = 0 ){
        this.#y = y;
        return this;
    }
}
export default Vertex;