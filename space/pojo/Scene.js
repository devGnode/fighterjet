import EventsPerforming from "./EventsPerforming.js";
import Factory          from "../factory/Factory.js";

class Scene{

    #history            = [];
    #historySelected    = 0x00;
    #eventsPerforming   = null;
    #cpu                = null;

    constructor( thread = null ) {
        this.#history   = [];
        this.#cpu       = thread;
        this.#eventsPerforming = new EventsPerforming(thread);
    }

    getAnimationsPerforming(){return this.#eventsPerforming;}

    getCpuThread(){ return this.#cpu; }

    getHistory(){ return this.#history; }

    nexHistory(){
        this.#historySelected = (this.#historySelected++)%this.#history.length;
        return this;
    }

    setInfiniteParty(){

    }

    bot( ){

    }

}
export default Scene;

/**

 history =>

 */