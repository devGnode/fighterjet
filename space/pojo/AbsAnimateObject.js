/***
 * AbstractAnimatedObject
 */
import Vertex   from "./Vertex.js";
import Textures from "../dat/Textures.js";

class AbsAnimatedObject{

    #destroyFrame = false;
    #display      = true;

    #framebuffer = null;
    #vertex      = new Vertex();
    #offset      = 0x05;

    #areaColor   = 0xffffff;

    constructor( frameBuffer = null, x = 0x00, y = 0x00, offset = 0x05 ) {
        this.#framebuffer = frameBuffer;
        this.#offset      = offset&0xf;
        this.#display     = true;
        this.#vertex.setX(x).setY(y);
    }

    /***
     * @return {*}
     * @constructor
     */
    get X( ){ return this.#vertex.x;}

    get Y( ){ return this.#vertex.y;}

    get width(){ return this.#framebuffer.getWidth(); }

    get height(){ return this.#framebuffer.getHeight(); }

    get offset(){ return this.#offset;}

    get destroyFrame(){ return this.#destroyFrame; }

    get displayFrame( ){ return this.#display;}

    getFrameBuffer( ){ return this.#framebuffer; }

    getPixel( x, y ){return this.#framebuffer.getPixel(x,y);}

    /***
     * @return {AbsAnimatedObject}
     */
    setDestroyFrame( ){
        this.#destroyFrame = true;
        return this;
    }

    /***
     * @param state
     * @return {AbsAnimatedObject}
     */
    setDisplay( state = true ){
        this.#display = state;
        return this;
    }

    /**
     * @param offset
     * @return {AbsAnimatedObject}
     */
    setOffset( offset = 0x00 ){
        this.#offset = offset;
        return this;
    }

    /***
     * @param frame
     * @return {AbsAnimatedObject}
     */
    setFrameBuffer( frame = null ){
        if(!(frame instanceof FrameBuffer)) throw new TypeError("Bad cast parameter only FrameBuffer is accepted");
        this.#framebuffer = frame;
        return this;
    }

    /**
     * @return {number}
     */
    getAreaColor(){ return this.#areaColor}

    /***
     * @param color
     * @return {AbsAnimatedObject}
     */
    setAreaColor( color = 0xffffff ){
        this.#areaColor = color;
        return this;
    }

    /**
     * @param x
     * @return {AbsAnimatedObject}
     */
    setX( x = 0 ){
        this.#vertex.setX(x);
        return this;
    }

    /***
     * @param y
     * @return {AbsAnimatedObject}
     */
    setY( y = 0){
        this.#vertex.setY(y);
        return this;
    }

    /***
     * @param x
     * @param y
     * @return {AbsAnimatedObject}
     */
    setPosition( x = 0, y = 0 ){
        this.#vertex.setX(x).setY(y);
        return this;
    }

    /***
     * replace %400 by screen width value
     * @param displacement
     * @return {AbsAnimatedObject}
     */
    setXDisplacement( displacement = -1 ){
        this.#vertex.setX((this.X+displacement)%400);
        return this;
    }

    /***
     * replace %650 by screen height value
     * @param displacement
     * @return {AbsAnimatedObject}
     */
    setYDisplacement( displacement = -1  ){
        this.#vertex.setY((this.Y+displacement)%650);
        return this;
    }

    /***
     * 0----1
     * 2----3
     * @param hasGap
     * @return {[]}
     */
    getVertexArea( hasGap = true ){
        let vertex = [], i = 0,
            middleX = parseInt(this.#framebuffer.getWidth()/2),
            middleY = parseInt(this.#framebuffer.getHeight()/2),
            gap =   hasGap ? this.#offset : 0;

        for(; i < 0x04; i++ ){
            vertex.push({
                x: this.#vertex.x + ( i === 0 || i === 2 ? -middleX : middleX ) + ( i === 0 || i === 2 ? -gap : gap ),
                y: this.#vertex.y + ( i === 0 || i === 1 ? -middleY : middleY ) + ( i === 0 || i === 1 ? -gap : gap ),
            });
        }

        return vertex;
    }

    /***
     * random lucky chance
     * to unlock animation
     * @param xLucky
     * @return {boolean}
     */
    getRandomLucky( xLucky = 2 ){
       return Math.round(Math.random() * xLucky) === Math.round(Math.random() * xLucky);
    }

    /***
     * @param skinNo
     * @return {null|AbsAnimatedObject}
     */
    setSkin( skinNo = 0x00 ){
        if(Textures[skinNo&=0xff]) return  this.setFrameBuffer(FrameBuffer.builder( Textures[skinNo].dat,{ x: Textures[skinNo].width, y: Textures[skinNo].height }));
        return null;
    }

}
/***
 * WebBrowserExporation
 */
export default AbsAnimatedObject;