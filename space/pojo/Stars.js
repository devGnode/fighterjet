import AbsAnimatedObject from "./AbsAnimateObject.js";
import Vertex            from "./Vertex.js";

/***
 * background frame
 */
class Stars extends AbsAnimatedObject{

    #vertex = [];

    #width;
    #height;

    #rate   = 0x0A;
    #colors = [0xCDFF8B,0xFFFFFF];

    constructor( monitor = null ) {
        super(null,-1,-1,0);
        this.#height = monitor.getFrameBuffer().getHeight();
        this.#width  = monitor.getFrameBuffer().getWidth();
        this.raz();
    }

    raz( ){
        this.#vertex = [];
        for(let i =1; i < this.#height; i++) this.gen(i);
    }

    setColorA( color = 0xFFFFFF ){
        this.#colors[0] = color;
        return this;
    }

    setColorB( color = 0xDCFF8B ){
        this.#colors[1] = color;
        return this;
    }

    getColors( ){return this.#colors;}

    /***
     * Private
     */
    gen( y = 0x00 ){
        let c    = Math.floor(Math.random()*(Math.random()*10)),
            step = Math.round(this.#width/c)+Math.round(Math.random()*10),
            len = Math.floor(Math.random()*(Math.random()*this.#width)),
            i = 1;

        for(; i < len; i+=step )this.#vertex.push(new Vertex(i,y));
    }

    /***
     * Callback called by EventsPerforming Object
     * @param animation
     * @param thread
     * @param frame
     */
    perform( animation = null, thread = null, frame = null ){
        let tmp = [],
        color0 = this.#colors[0],
        color1 = this.#colors[1];

        this.gen();
        if(this.#vertex.length>0)
        tmp = this.#vertex.filter(value=>{
            let bool;

            if((bool=value.y<this.#height)) {
                frame.setPixel(value.x+Math.round(Math.random()*this.#width), value.setY( value.y+2 ).y, color0)
                     .setPixel(value.x+Math.round(Math.random()*this.#width),value.y, color1);
            }
            return bool;
        });
        this.#vertex = tmp;

        return false;
    }

}

/***
 * export
 */
export  default Stars;