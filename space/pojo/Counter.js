import AbsAnimatedObject from "./AbsAnimateObject.js";
import Textures          from "../dat/Textures.js";

class Counter extends AbsAnimatedObject{

    static COUNTER_SIZE = 5;
    static NUMBER_GAP   = 2;
    #player;

    constructor( player = null ) {
        super(null );
        this.#player = player;
    }

    getSprite( no = 0x00 ){
        return FrameBuffer.builder(Textures[0x50].dat[no],{x: Textures[0x50].width ,y: Textures[0x50].height });
    }

    getNum( ){return new Array(Counter.COUNTER_SIZE).fill(this.getSprite(0));}

    /***
     * @param animation
     * @param thread
     * @param frame
     * @return {boolean}
     */
    perform( animation = null, thread = null, frame = "" ){
        let tile = frame.tiles()
                        .setOffsetX(10)
                        .setOffsetY(12),
            offsetX = 10, i = 0,
            numbers = this.getNum(),
            value = this.#player.getReward();

       // value
       while( value > 0  ){
           numbers[i] = this.getSprite(parseInt(value%10) );
           value = parseInt(value/10);
           i++;
       }
      numbers.reverse().forEach(number=>{
            tile.setTile(offsetX,626,number.getUin32FrameBuffer());
            offsetX+=(10+Counter.NUMBER_GAP);
        });

        return this.destroyFrame;
    }

}
export default Counter;