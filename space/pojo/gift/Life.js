import InfiniteLife from "./InfiniteLife.js";

class Life extends InfiniteLife{

    constructor( x = 0x00, y = 0x00, life = 0x00 ){
        super( x, y );
        super.setSkin(0x22);
        this.setLife = (life == null && life <= 0x00) ? Math.round(Math.random()*100) : life;
    }

    get getLife( ){ return super.getLife; }

}
export default Life;