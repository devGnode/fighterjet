import AbsAnimatedObject from "../AbsAnimateObject.js";

class Gift extends AbsAnimatedObject{

    static DEFAULT_DURATION = 5000;

    #statFrame = new Date().getTime();
    #duration  = 0x00;

    constructor( tileNo = null, x = 0x00, y = 0x00, duration = Gift.DEFAULT_DURATION  ) {
        super(null, x, y, 0x00);
        this.#duration = duration;
        if( tileNo !== null ) this.setSkin(tileNo);

    }

    get startFrame( ){ return this.#statFrame; }

    getElapsedTime( ){return new Date().getTime()-this.startFrame;}

    /***
     * @param animation
     * @param thread
     * @param frame
     * @param elements
     */
    perform( animation = null, thread = null, frame = null,  elements = null ){

        this.setYDisplacement(+1);
        if(this.getElapsedTime()>this.#duration) this.setDestroyFrame();

        return this.destroyFrame;
    }

}
export default Gift;