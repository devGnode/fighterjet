import Gift              from "./Gift.js";

class BulletsMagazine extends Gift{

    #bullets = 0x00;
    #caliber = 9.00;

    constructor( tile = null, bullets = 0x00, caliber = 9.00, x = 0x00, y = 0x00) {
        super( tile||0x31, x, y );
        this.#bullets = bullets;
        this.#caliber = caliber;
    }

    get bulletsValue( ){ return this.#bullets; }

    get caliber( ){ return this.#caliber; }

}
export default BulletsMagazine;
