import Gift from "./Gift.js";

class InfiniteLife extends Gift{

    static LUCKY_FALL = 10000;

    #life = Number.POSITIVE_INFINITY;

    constructor( x = 0x00, y = 0x00, duration = 5 ) {
        super( 0x21, x, y, duration*1000 );
    }

    get getLife( ){ return this.#life; }

    set setLife( life ){
        this.#life = life;
    }

}
export default InfiniteLife;