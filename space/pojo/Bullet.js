import AbsAnimatedObject from "./AbsAnimateObject.js";
import Textures from "../dat/Textures.js";

class Bullet extends AbsAnimatedObject{

    #owner          = null;
    #weaponHandler  = null;

    // > 0 up
    // < 0 down
    #speed      = 0x10;
    #damage     = 0x00;
    #direction  = 0x01;

    constructor( speed = 0.00, damage = 0.00, x = 0x00, y = 0x00, tile = 0x10, degree = +1 ) {
        let frame = null;
        if(tile>0) frame = FrameBuffer.builder(Textures[tile].dat,{x:Textures[tile].width,y:Textures[tile].height})
            .rotate(degree>0?90:-90);
        super( frame, x, y, 0x00 );
        this.#speed     = degree>0?-speed:speed;
        this.#direction = degree>0?90:-90;
        this.#damage    = damage;
    }

    /***
     * @return {number}
     */
    get damage( ){return this.#damage}

    /***
     * @return {number}
     */
    get speed( ){ return this.#speed; }

    /***
     * @param fighterJet
     * @return {Bullet}
     */
    setOwner( fighterJet = null ){
        this.#owner = fighterJet;
        return this;
    }

    /***
     * @return {null}
     */
    getOwner(){return this.#owner;}

    /***
     * @param owner
     * @return {boolean}
     */
    isMine( owner = null ){return owner === this.#owner;}

    /***
     * Callback called by EventsPerforming Object
     * @param animation
     * @param thread
     */
    perform( animation = null, thread = null ){

        if(this.destroyFrame )return true;

        this.setY(this.Y+this.#speed);
        let bulletSize = this.getFrameBuffer().getHeight();

        if ( (this.#speed > 0 && this.Y >= 650 + bulletSize ) ||  (this.#speed < 0 && this.Y <= -bulletSize)){
            this.setDestroyFrame();
        }
        return this.destroyFrame;
    }

}

export default Bullet;