import Weapon from "./Weapon.js";

class M230 extends Weapon{

    static NAME     = M230.name;
    static MAG_MAX  = 20;

    constructor( loader = 20) {
        super(
            loader,
            M230.MAG_MAX,
            11.0,
            0.30,
            1.00,
            0.1,
            0x12
        );
    }

    getName(){return M230.NAME;}

}
export default M230;