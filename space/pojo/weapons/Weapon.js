/***
 * Weapon class Object
 */
import Bullet          from "../Bullet.js";
import BulletsMagazine from "../gift/BulletsMagazine.js";

class Weapon{

    #loader      = [Number.POSITIVE_INFINITY,Number.POSITIVE_INFINITY];
    #caliber     = 9.0;
    #velocity    = 0;
    #damage      = 0;

    #rate        = 0.05;
    #next        = (new Date()).getTime();
    #bulletTile  = 0x10;

    #direction   = +1;

    constructor( loader = Number.POSITIVE_INFINITY, maxLoader = Number.POSITIVE_INFINITY, velocity = 0, caliber = 9.0, damage = 0.5, rate = 0.05, tile = 0x10 ) {
        this.#loader   = [loader,maxLoader];
        this.#velocity = velocity;
        this.#caliber  = caliber;
        this.#damage   = damage;
        this.#rate     = rate;
        this.#bulletTile=tile;
    }

    get loader(){ return this.#loader; }

    get caliber( ){ return this.#caliber; }

    get velocity(){ return this.#velocity; }

    get damage(){ return this.#damage; }

    /***
     * @return {number}
     */
    get rate(){return Math.round(this.#rate*1000 ); }

    /***
     * @param rate
     * @return {Weapon}
     */
    setRate( rate = 0x00 ){
        this.#rate = rate;
        return this;
    }

    /***
     * default +1
     * @param value
     * @return {Weapon}
     */
    setDirection( value = +1 ){
        this.#direction = value>0?value&0x01:-(value&0x1);
        return this;
    }

    /***
     * @return {boolean}
     */
    canShoot( ){return (this.#next && (new Date().getTime()-this.#next) >this.rate);}

    /***
     * @param magazine
     * @return {boolean}
     */
    addMagazine( magazine = null ){
        if( magazine instanceof BulletsMagazine && magazine.caliber === this.#caliber ) {
            if(!isNaN(this.#loader[1] - this.#loader[0] ) && this.#loader[1] - this.#loader[0] > 0 ) {
                this.#loader[0] += Math.abs(magazine.bulletsValue);
                magazine.setDestroyFrame();
                return true;
            }
        }
        return false;
    }

    getLoaderBullet( ){

    }

    /***
     * @param owner of the bullet
     * @return {null|Bullet}
     */
    shoot(owner = null){
        let loaderValue = this.#loader[0],north,
        can = this.canShoot();

        if(loaderValue<=0)if(owner.getWeapons().map(v=>v.loader>0).length>0) owner.nextWeapon().shoot(owner);
        if(loaderValue>0&&can) {
            this.#next = (new Date()).getTime();
            if(!(this.#loader[0]===Number.POSITIVE_INFINITY)){
                this.#loader[0]--;
                this.#loader[0]=this.#loader[0]<=0?0:this.#loader[0];
            }
            north = Math.round(owner.getFrameBuffer().getHeight()/2);
            return (new Bullet(
                this.#velocity,
                this.#damage,
                owner.X,
                owner.Y+((this.#direction>=0)?-north:north),
                this.#bulletTile,
                this.#direction
            )).setOwner(owner);
        }

        return null;
    }

    /***
     * @param object
     * @return {boolean}
     */
    hasBulletInstance( object = null ){
        return object instanceof Bullet;
    }

}
export default Weapon;