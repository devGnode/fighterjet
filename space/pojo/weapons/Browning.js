import Weapon from "./Weapon.js";

class Browning extends Weapon{

    static NAME = Browning.name;

    constructor( loader = 150) {
        super(
            loader||150,
            20,
            10.0,
            0.50,
            0.5,
            0.1,
            0x13
        );
    }

    getName(){return Browning.NAME;}

}
export default Browning;