import Weapon from "./Weapon.js";

class WarHead extends Weapon{

    static NAME     = WarHead.name;
    static MAG_MAX  = 30;

    constructor( magazine = 30 ) {
        super(
            magazine,
            WarHead.MAG_MAX,
            10.0,
            55.0,
            1.00,
            0.12,
            0x11
        );
    }

    getName(){return WarHead.NAME;}
}
/***
 */
export default WarHead;