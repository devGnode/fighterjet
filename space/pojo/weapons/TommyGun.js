import Weapon from "./Weapon.js";
/***
 *
 */
class TommyGun extends Weapon{

    static NAME     = TommyGun.name;
    static MAC_MAX  = Number.POSITIVE_INFINITY;

    constructor( bullets = Number.POSITIVE_INFINITY ) {
        super(
            bullets,
            TommyGun.MAC_MAX,
            10.0,
            20.00,
            0.2,
            0.02,
            0x10,
        );
    }

    getName(){return TommyGun.NAME;}

}

/***
 */
export default TommyGun;