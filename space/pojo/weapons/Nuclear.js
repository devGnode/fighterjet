import Weapon         from "./Weapon.js";
import NuclearWarHead from "../NuclearWarHead.js";

class Nuclear extends Weapon{

    static NAME     = Nuclear.name;
    static MAG_MAX  = 0x05;

    constructor( magazine = 0x00 ) {
        super(
            magazine,
            Nuclear.MAG_MAX,
            20.0,
            120.00,
            1.00,
            1,
            0x10
        );
    }

    getName(){return Nuclear.NAME;}

    /**
     * @Override method
     * @param owner of the bullet
     * @return {null|Bullet}
     */
    shoot(owner = null){
       let bullet;
       if( (bullet = super.shoot(owner) ) ) {
           return new NuclearWarHead(this.damage).setOwner(bullet.getOwner());
       }
       return null;
    }

}
export default Nuclear;