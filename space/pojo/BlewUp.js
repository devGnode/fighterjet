import AbsAnimatedObject from "./AbsAnimateObject.js";
import Textures          from "../dat/Textures.js";

class BlewUp extends AbsAnimatedObject{

    #start = new Date().getTime();
    #rate  = 0x00;
    #sprite= 0x00;
    #len   = 0x00;
    #next  = -1;

    constructor( x = 0x32, y = 0x96, seconds = 0.5 ) {
        super(
            null,
            x, y,
            0
        );
        this.#rate = Math.round((seconds*1000)/Textures[0x20].dat.length);
        this.#len  = Textures[0x20].dat.length;
        this.setSkin(0x00);
    }

    /***
     * @Override method
     * @param skinNo
     * @return {BlewUp|null}
     */
    setSkin( skinNo = 0x00 ){
        if(Textures[0x20].dat[skinNo&=0xff])return this.setFrameBuffer(FrameBuffer.builder( Textures[0x20].dat[skinNo],{ x: Textures[0x20].width, y: Textures[0x20].height }));
        return null;
    }

    /***
     * Callback called by EventsPerforming Object
     * @param animation
     * @param thread
     * @param frame
     */
    perform( animation = null, thread = null, frame = null){
        let time = new Date().getTime();

        let p = Math.round((((time-this.#start )/100)%this.#rate));
        if( p===this.#len-1 ){
            this.#start = new Date().getTime();
            this.#next  = -1;
            this.setDestroyFrame();
        }
        if( this.#next !== p ){
            this.setSkin(p);
            this.#next = p;
        }
        // 13

        return this.destroyFrame;
    }

}
/***
 *
 */
export default BlewUp;