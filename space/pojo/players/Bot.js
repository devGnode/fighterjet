import FighterJet      from "../FighterJet.js";
import Bullet          from "../Bullet.js";
import BlewUp          from "../BlewUp.js";
import BulletsMagazine from "../gift/BulletsMagazine.js";
import Life            from "../gift/Life.js";
import {Collider} from "../../../Collider.js";

class Bot extends FighterJet{

    #rewardValue = 0x00;
    #weaponRate  = 0x00;
    #direction      = Collider.SOUTH_OR;

    constructor(  sprite = null,  name = "????", x = 0, y = 0, speed = 0, weaponRate = 1.00, offset = 0x05 ) {
        super(
            null, name,
            x, y,
            speed, offset
        );
        super.setSkin(sprite);
        super.getFrameBuffer().rotate(180);
        this.#weaponRate = weaponRate;
    }

    getDirection( ){ return this.#direction; }

    get reward(){ return this.#rewardValue; }

    // @Override
    setWeapon(weapon = null) {
        return super.setWeapon(weapon.setDirection(-1).setRate(this.#weaponRate));
    }

    // @Override
    setDamage(bullet = null) {
        super.setDamage(bullet);
        return this;
    }

    /***
     * Callback called by EventsPerforming Object
     * @param animation
     * @param thread
     * @param frame
     * @param elements
     */
    perform( animation = null, thread = null, frame = null, elements = null ){

        // if this.Y > screenY then  this.setDestroyFrame();
        this.setYDisplacement(+1);

        // life
        if(this.getCurrentLife()<=0){
            animation.push(new BlewUp(this.X,this.Y));
            if(this.getRandomLucky(10)){
                animation.push(new BulletsMagazine(0x00,parseInt(Math.random()*100),20.00,this.X,this.Y));
            }else if(this.getRandomLucky(20)){
                animation.push(new Life(this.X,this.Y,null));
            }
            this.setDestroyFrame();
        }

       elements.forEach(object=>{

           if( object instanceof FighterJet && !(object instanceof Bot) ){

               switch (Collider.orientation(object,this)) {
                   case Collider.NORTH_WEST_OR: console.log("BOT NORTH WEST"); break;
                   case Collider.EAST_OR: console.log("BOT RIGHT"); break;
                   case Collider.SOUTH_EAST_OR: console.log("BOT SOUTH_EAST"); break;

                   case Collider.NORTH_OR: console.log("BOT FRONT"); break;
                   case Collider.SOUTH_OR: console.log("BOT BEHIND"); break;
                   case Collider.NORTH_EAST_OR: console.log("BOT NORTH EAST"); break;
                   case Collider.WEST_OR: console.log("BOT KEFT"); break;
                   case Collider.SOUTH_WEST_OR: console.log("BOT SOUT WEST"); break;
               }

               let v = object.getVertexArea(false ),
                   m = this.getVertexArea(false );

               if( (v[0].x-m[3].x <= 0 && m[2].x-v[1].x <=0 ) && (v[0].y-m[3].y >= 0 && m[1].y-v[2].y <= 0) ){
                   let tmp;
                   if((tmp=this.shoot())) animation.push(tmp);
               }
           }
           if( object instanceof Bullet && !object.isMine(this) && !(object.getOwner() instanceof Bot) ){
                let v = object.getVertexArea(true ),
                    m = this.getVertexArea(true );

                if( (v[0].x-m[3].x <= 0 && m[2].x-v[1].x <=0 ) && (v[0].y-m[3].y <= 0&&m[1].y-v[2].y <= 0 ) ) {
                   this.setDamage(object);
                   object.setDestroyFrame();
                }
           }

        });

        // set animation destroy
        return this.destroyFrame;
    }

}
export default Bot;