import FighterJet      from "../FighterJet.js";
import KeyboardEvents  from "../../lib/KeyboardEvents.js";
import Keypad          from "../../lib/Keypad.js";
import BlewUp          from "../BlewUp.js";
import Bullet          from "../Bullet.js";
import BulletsMagazine from "../gift/BulletsMagazine.js";
import InfiniteLife    from "../gift/InfiniteLife.js";
import Life            from "../gift/Life.js";
import {Collider} from "../../../Collider.js";

class Player extends FighterJet{

    #keyboardEvents = KeyboardEvents.getInstance();
    #weaponSight    = false;
    #backward       = 0x05;
    #direction      = Collider.NORTH_OR;

    /****
     * to se
     * @type {number}
     */
    #startFrame     = new Date().getTime();

    constructor( sprite = 0x00,  name = "????", x = 0, y = 0, speed = 0 ) {
        super(
            null,
            name,
            x,
            y,
            speed,
            0x05,
            0
        );
        super.setSkin(sprite);
    }

    getDirection( ){ return this.#direction; }

    setWeaponSight( state = true ) {
       this.#weaponSight = state;
       return this;
    }

    hasWeaponSight(){ return this.#weaponSight; }

    setBackward( value = 0x00 ){
        this.#backward = value;
        return this;
    }

    backward( state = true ){
        this.#backward = state? Math.abs( this.#backward ) : -this.#backward;
        return this;
    }

    getBackwardState(){ return this.#backward>0; }

    /****
     * millisecond
     * @return {number}
     */
    getElapsedTime( ){
        return this.#startFrame-(new Date().getTime());
    }

    /***
     * @param ms
     * @return {boolean}
     */
    isElapsedTime( ms = 0x00 ){
        return ms > this.getElapsedTime();
    }

    /***
     * Callback called by EventsPerforming Object
     * @param animation
     * @param thread
     * @param frame
     * @param elements
     */
    perform( animation = null, thread = null, frame = null, elements = null ){
        let keysPressed;

        //console.log(thread.getClock, parseInt((thread.getClock-this.#startFrame)/1000));
        // Displacement
        if((keysPressed = this.#keyboardEvents.getKeysPressed()).length>0){

            // arrow displacement
            if(keysPressed.has(Keypad.VK_LEFT)) this.setXDisplacement(-1);
            if(keysPressed.has(Keypad.VK_RIGHT)) this.setXDisplacement(+1);
            if(keysPressed.has(Keypad.VK_UP)) this.setYDisplacement(-1);
            if(keysPressed.has(Keypad.VK_DOWN)) this.setYDisplacement(+1);

            // shoot
            if(keysPressed.has(Keypad.VK_SHOOT)){
                let bullet;
                if((bullet = this.shoot())) animation.push( bullet );
            }
            // CHOICE WEAPON, get interrupt each 6ms
            if(keysPressed.has(Keypad.VK_WEAPON_CHOICE)&& !this.#keyboardEvents.getKeyProperties(Keypad.VK_WEAPON_CHOICE).repeat ) this.nextWeapon();

            // PAUSE & START
            if(keysPressed.has(KeyboardEvents.VK_SHIFT)&&keysPressed.has(0x53)&&!this.#keyboardEvents.getKeyProperties(0x53).repeat){
              this.setWeaponSight( !this.#weaponSight );
            }

            if(keysPressed.has(KeyboardEvents.VK_SHIFT)&&keysPressed.has(0x41)&&!this.#keyboardEvents.getKeyProperties(0x41).repeat) {
                this.backward( !this.getBackwardState() );
            }

            if(keysPressed.has(KeyboardEvents.VK_SHIFT)&&keysPressed.has(0x49)&&!this.#keyboardEvents.getKeyProperties(0x49).repeat) {
                this.setInvincibility( !this.getInvincibility() );
            }

        }

        /***
         * replace 650 by height screen object
         */
        if( this.#backward >0 && !keysPressed.has(Keypad.VK_UP) && this.Y <= (650 - ( (this.getFrameBuffer().getHeight()/2) + this.offset ) ) ){
           this.setY( this.Y + Math.round( this.speed/this.#backward ) );
        }

        // life
        if(this.getCurrentLife()<=0){
            animation.push(new BlewUp(this.X,this.Y));
            this.setDestroyFrame();
        }
        // bink
        // if(thread.getTick%10===1) this.setDisplay( !this.displayFrame );

        elements.forEach(object=>{
            if( object instanceof FighterJet && object !== this ){
               let v = object.getVertexArea(false ),
                    m = this.getVertexArea(false);

               // console.log(v[0].x-m[3].x, m[2].x-v[1].x,v[0].y-m[3].y, m[1].y-v[2].y )

                switch (Collider.orientation(object,this)) {
                    case Collider.NORTH_WEST_OR: console.log("NORTH WEST"); break;
                    case Collider.EAST_OR: console.log("EAST"); break;
                    case Collider.SOUTH_EAST_OR: console.log("SOUTH_EAST"); break;

                    case Collider.NORTH_OR: console.log("FRONT"); break;
                    case Collider.SOUTH_OR: console.log("BEHIND"); break;
                    case Collider.NORTH_EAST_OR: console.log("NORTH EAST"); break;
                    case Collider.WEST_OR: console.log("WEST"); break;
                    case Collider.SOUTH_WEST_OR: console.log("SOUT WEST"); break;
                }
              // if( (v[0].x-m[3].x <= 0 && m[2].x-v[1].x <=0 ) && (v[0].y-m[3].y <= 0&&m[1].y-v[2].y <= 0 ) ){
                if(Collider.hasCollision(object,this, true)){
                  // console.log(v[0].x-m[3].x, m[2].x-v[1].x, v[0].y-m[3].y, m[1].y-v[2].y )
                   // alert("ok")
                   // this.setDamage(this);
                  //  object.setDamage(object);
                   // alert("ok")
               }
                switch (Collider.orientation(object,this)) {
                    case Collider.NORTH_WEST_OR: console.log("NORTH WEST"); break;
                    case Collider.EAST_OR: console.log("RIGHT"); break;
                    case Collider.SOUTH_EAST_OR: console.log("SOUTH_EAST"); break;

                    case Collider.NORTH_OR: console.log("FRONT"); break;
                    case Collider.SOUTH_OR: console.log("BEHIND"); break;
                    case Collider.NORTH_EAST_OR: console.log("NORTH EAST"); break;
                    case Collider.WEST_OR: console.log("LEFT"); break;
                    case Collider.SOUTH_WEST_OR: console.log("SOUT WEST"); break;
                }

                /*if(Collider.orientation(object,this)===Collider.NORTH_OR) {
                    let bullet;
                    if((bullet = this.shoot())) animation.push( bullet );
                }*/
               if(this.#weaponSight)
               if(Collider.orientation(object,this)===Collider.NORTH_OR) {
                   object.setAreaColor(0x00ff00);
                    frame.drawSquareArea(object);
               }else if(Collider.orientation(object,this)===Collider.SOUTH_OR){
                   object.setAreaColor(0xff0000);
                   frame.drawSquareArea(object);
               }
            }
            if( object instanceof Bullet && !object.isMine(this)){
                let v = object.getVertexArea(true ),
                    m = this.getVertexArea(true );

                if( (v[0].x-m[3].x <= 0 && m[2].x-v[1].x <=0 ) && (v[0].y-m[3].y <= 0&&m[1].y-v[2].y <= 0 ) ) {
                    this.setDamage(object);
                    object.setDestroyFrame();
                }
            }
            if(object instanceof BulletsMagazine || object instanceof InfiniteLife  ){
                let v = object.getVertexArea(false ),
                    m = this.getVertexArea(false );

                if((v[0].x-m[3].x <= 0 && m[2].x-v[1].x <=0 ) && (v[0].y-m[3].y <= 0&&m[1].y-v[2].y <= 0 )){

                    if(object instanceof BulletsMagazine) {
                        let tmp = this.getWeapons().filter(weapon => weapon.caliber === object.caliber);
                        if (tmp.length > 0) {
                            //Math.round(Math.random()*tmp.length-1)
                            tmp[0].addMagazine(object);
                        }
                    }else if((object instanceof InfiniteLife) || (object instanceof  Life) ){
                        console.log("dsqd");
                        this.setLife(object);
                    }
                }
            }
        });
        return this.destroyFrame;
    }


}
export default Player;