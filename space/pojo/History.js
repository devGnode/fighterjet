/***
 *
 */
import Utils from "../utils/Utils.js";

class History{

    #scene      = null;
    #startFrame = (new Date()).getTime();
    #duration   = Number.POSITIVE_INFINITY;

    #botSprite       = [];
    #bots            = Number.POSITIVE_INFINITY;
    #botsSpeed       = 0x01;
    #nextSpawnBot    = (new Date()).getTime();
    #botsElapsedTime = 5000;
    #botRifleRate    = 0.5;
    #botsWeapons     = [];
    #botsAreaGap     = 0x05;
    #botsWeaponsRand = false;

    #player     = 0;

    #giftXLucky = 1000;

    constructor( scene = null ) {
        this.#scene = scene;
    }

    getElapsedTime( ){return (new Date().getTime())-this.#startFrame;}

    isDie( ){ return this.#player.getCurrentLife() <= 0; }

    isEnded( ){ return this.getElapsedTime()/1000 >= this.#duration;  }

    spawnBot( ){
        let time;
        if( (time=(new Date()).getTime())-this.#nextSpawnBot >= this.#botsElapsedTime){
            this.#nextSpawnBot = time;
            return 1;
        }else if( Utils.getRandomLucky(500  ) ){
            return Math.round(Math.random()*5);
        }
        return 0;
    }

    canSpawnGift( ){return Utils.getRandomLucky(this.#giftXLucky);}

    static from(){

    }

}
export default History;