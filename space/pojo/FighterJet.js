/***
 * import
 */
import AbsAnimatedObject from "./AbsAnimateObject.js";
import Weapon            from "./weapons/Weapon.js";
import Textures          from "../dat/Textures.js";

class FighterJet extends AbsAnimatedObject{

    #name            = "????";
    #degree          = 0x00;

    #invincibility   = false;
    #speed           = 0x00;
    #life            = 0x00;

    #weapons         = [];
    #selectedGun     = 0x00;

    #token           = 0x00;
    #damage          = 1.00;

    constructor( frame = null,  name = "????", x = 0, y = 0, speed = 0, offset = 0x05, degree = 90 ) {
        super( frame,  x, y, offset);
        this.#name        = name;
        this.#degree      = degree;
        this.#life        = 100;
        this.#speed       = speed;
    }

    get speed(){ return this.#speed;}

    get getName(){return this.#name;}

    setXDisplacement(displacement = -1) {
        return super.setXDisplacement(displacement>0?this.#speed:-this.#speed);
    }

    setYDisplacement(displacement = -1) {
        return super.setYDisplacement(displacement>0?this.#speed:-this.#speed);
    }

    getInvincibility(){ return this.#invincibility;}

    setInvincibility( state = false ){
        this.#invincibility = state;
        return this;
    }

    get damage( ){return this.#damage }

    setDamage( bullet = null ){
        if( !this.#invincibility && this.#life > 0 ){
            this.#life -= Math.round(bullet.damage*100);
            this.#life = this.#life<0?0:this.#life;
            if(bullet.getOwner) bullet.getOwner().addReward(1);
        }
        return this;
    }

    getCurrentLife( ){ return this.#life; }

    setLife( life = null ){ this.#life += life.getLife; life.setDestroyFrame(); return this; }

    shoot( ){ return this.#weapons[this.#selectedGun||0x00].shoot(this); }

    selectWeapon( no = 1){
        if( this.#weapons[no] ) this.#selectedGun = no;
    }

    nextWeapon(){
        this.#selectedGun++;
        if(this.#selectedGun>=this.#weapons.length)this.#selectedGun=0;
        return this;
    }

    setWeapon( weapon ){
        if( weapon instanceof Weapon )this.#weapons.push(weapon);
        return this;
    }

    getWeapons( ){return this.#weapons;}

    getWeaponSelected( ){
        if(this.#weapons[this.#selectedGun]){
            return this.#weapons[this.#selectedGun];
        }
        return null;
    }

    // @Override
    setSkin( skinNo = 0x00 ){
        if(Textures[skinNo&=0x0f]) return  this.setFrameBuffer(FrameBuffer.builder( Textures[skinNo].dat,{ x: Textures[skinNo].width, y: Textures[skinNo].height }));
        return null;
    }

    addReward( token = 0x00 ){
        this.#token += token;
        return this;
    }

    getReward( ){return this.#token; }

 }
/***
 * WebBrowserExportation
 */
export default FighterJet;