import KeyboardEvents from "/jsnake/space/lib/KeyboardEvents.js";
import Thread from "/jsnake/space/lib/Thread.js";
import Keypad from "/jsnake/space/lib/Keypad.js";

//
import TommyGun         from "./pojo/weapons/TommyGun.js";
import WarHead          from "./pojo/weapons/WarHead.js";
import M230             from "./pojo/weapons/M230.js";
import Browning         from "./pojo/weapons/Browning.js";
import Player           from "./pojo/players/Player.js";
import Bot              from "./pojo/players/Bot.js";
import EventsPerforming from "./pojo/EventsPerforming.js";
import Stars            from "./pojo/Stars.js";
import BlewUp           from "./pojo/BlewUp.js";
import TestTile         from "./pojo/test/TestTile.js";
import BulletsMagazine  from "./pojo/gift/BulletsMagazine.js";
import Nuclear          from "./pojo/weapons/Nuclear.js";
import Counter          from "./pojo/Counter.js";
import InfiniteLife     from "./pojo/gift/InfiniteLife.js";
import Life             from "./pojo/gift/Life.js";
import Factory          from "./factory/Factory.js";

class SpaceX{

    static FRAME_RATE = 25;
    static CLOCK      = 60;

    static #DIM_X      = 400;
    static #DIM_Y      = 650;

    #gui     = null;
    #rate    = null;
    #cpuSnap = null;
    #threads = [];

    // keypad
    #keypad = Keypad.INSTANCE;

    // tmp
    player;

    constructor( monitor = null ) {
        this.#gui = monitor.resize(SpaceX.#DIM_X,SpaceX.#DIM_Y);
        this.#threads.push(new Thread(Math.round(1000/SpaceX.FRAME_RATE)));
        this.#threads.push(new Thread(Math.round(1000/SpaceX.CLOCK)));
    }

    static getKeyboardEvents(){ return KeyboardEvents.getInstance();}

    getKeypad( ){ return this.#keypad; }

    getGpuRate( ){ return this.#threads[0].frameRate; }

    getInstructionRate( ){ return this.#threads[1].frameRate; }

    start(){ this.#threads[1].next(); }

    pause( ){ this.#threads[1].pause(); }

    setSize( width = 400, height = 650 ){
        SpaceX.#DIM_X = width;
        SpaceX.#DIM_Y = height;
        this.#gui.resize(width,height);
    }

    end(){
        this.pause().raz();
        this.#threads[1].pause().raz();
    }

    onRefresh( callback = null ){
        this.#rate = callback;
        return this;
    }

    onCPUSnap( callback = null ){
        this.#cpuSnap = callback;
        return this;
    }

    getPlayer(){ return this.player; }

    tt= new Date().getTime();
    init( ){
        let gpu = this.#threads[0],
            cpu = this.#threads[1],
            kbe = KeyboardEvents.getInstance();

        // tmp
        let animation  = new EventsPerforming(cpu);
        let fighterJet = new Player(0x04,"Joey",200,200,8);
        let bot        = new Bot(0x02,"john",50,0,4,0x05);
        let bot2       = new Bot(0x02,"john2",200,0,1,1.5);

        this.player = fighterJet
            .setWeapon(new TommyGun(0))
            .setWeapon(new WarHead(20))
            .setWeapon(new M230(500)) //Number.POSITIVE_INFINITY
            .setWeapon(new Browning(1000)) //Number.POSITIVE_INFINITY
            .setWeapon(new Nuclear(Number.POSITIVE_INFINITY)) // Number.POSITIVE_INFINITY
            .setAreaColor();
        animation.set(new Stars(this.#gui)).set(new BlewUp()).set(fighterJet).set(bot2.setWeapon(new WarHead(Number.POSITIVE_INFINITY)).setInvincibility(!true));//.set(bot.setWeapon(new TommyGun().setDirection(-1)));
        animation.set(new Counter(fighterJet));
        console.warn(fighterJet);
        /**
         *
         * TileTest
         */
        //animation.set(new InfiniteLife(150,250));
        animation.set(new Life(150,150,null));
        //animation.set(new TestTile(0x21));

        cpu.callback(()=>{
            /*
            * set FrameBuffer
            * */
           /* if( (new Date().getTime())-this.tt > 200 ){
                animation.set(new Bot(0x02,"john2",parseInt(Math.random()*140)+60,0,1,1).setWeapon(Factory.infiniteWeapon((["M230","TommyGun","Browning"])[Math.round(Math.random()*3)])));
                animation.set(new Bot(0x02,"john2",parseInt(Math.random()*140)+60,0,1,1).setWeapon(Factory.infiniteWeapon((["M230","TommyGun","Browning"])[Math.round(Math.random()*3)])));
                this.tt = new Date().getTime();
            }*/
 
            gpu.setFork( animation.perform() );
            if( kbe.getKeysPressed().map(key=>{kbe.getKeyProperties(key).repeat = true; return key;}).has(Keypad.VK_START)){
               if( !cpu.isPause() ) cpu.pause();
            }
            if(this.#cpuSnap) this.#cpuSnap.call( this, this);
        });
        gpu.callback((self)=>{
            /***
            * refresh monitor : default 25 framerate / scd
            */
            this.#gui.refresh(self.getFork());
            if(this.#rate) this.#rate.call( this, this);
        });

        cpu.next();
        gpu.next();

        return this;
    }

}
/*
* KeyboardEvents
* */
window.SpaceX = SpaceX;