class Thread{

    #interval   = 0x00;
    #clock      = 0x00;
    #tick       = 0x00;
    #rate       = 0x00;
    #id         = null;

    #fork     = null;
    fCallback = null;

    constructor(rate = 0) {
        this.#interval = rate;
        this.raz();
    }
    /***
     * @return {number}
     */
    get frameRate( ){return this.#rate;}
    /**
     * @return {number}
     */
    get getTick(){ return this.#tick; }

    /**
     * @return {number}
     */
    get getClock(){return this.#clock;}

    /***
     * @return {Thread}
     */
    raz( ){
        this.#clock = (new Date()).getTime();
        this.#tick  = 0;
        return this;
    }
    /***
     * @return {Thread}
     */
    next(){
        let next = false;

        if(this.#id!==null) return this;
        this.#id = setInterval(()=>{
            if ((new Date()).getTime() - this.#clock >= 1000) {
                this.#rate = this.#tick;
                this.raz();
            }
            if(!next) {
                next = true;
                this.#tick++;
                this.fCallback(this);
                next=false;
            }
        },this.#interval);

        return this;
    }
    /***
     * @param callback
     * @return {Thread}
     */
    callback( callback = null ){
        this.fCallback = callback;
        return this;
    }
    /***
     * @return {Thread}
     */
    pause( ){
        clearInterval(this.#id);
        this.#id=null;
        return this;
    }
    /***
     * @return {boolean}
     */
    isPause( ){ return this.#id === null; }
    /***
     * @param fork
     * @return {Thread}
     */
    setFork( fork = null ){
        this.#fork = fork;
        return this;
    }
    /***
     * @return {null}
     */
    getFork(){return this.#fork;}

}
export default Thread;