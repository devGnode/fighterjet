/***
 * KeyboardEvents Singleton.
 * 0x00 - 0xFF => Uint8Array
 */
import Uint8 from "../utils/Uint8.js";

class KeyboardEvents{

    static INSTANCE = new KeyboardEvents();

    /***
     * Keyboard touch
     * @type {number}
     */
    static VK_LEFT_ARROW    = 0x25;
    static VK_UP_ARROW      = 0x26;
    static VK_RIGHT_ARROW   = 0x27;
    static VK_DOWN_ARROW    = 0x28;
    static VK_ENTER         = 0x0D;
    static VK_SPACE         = 0x20;
    static VK_SHIFT         = 0x10;
    static VK_CTRL          = 0x11;

    #pressed     = new Uint8Array(0xff).fill(0x00);
    #pressedProp = {};

    constructor() {
        if(KeyboardEvents.INSTANCE) return KeyboardEvents.INSTANCE;

        // keys properties
        for(let i =0; i < 255; i++ ) this.#pressedProp[i]={
            now:0, timestamp: 0, keyCode: i, repeat: false
        };

        document.body.addEventListener("keydown", (e)=>{
            let code = e.which||e.keyCode;
            this.#pressed[code] = 1;
            if(!(e.repeat||this.#pressedProp[code].repeat)) this.#pressedProp[code].timestamp = new Date().getTime();
            this.#pressedProp[code].now = new Date().getTime();
            e.preventDefault();
        });
        document.body.addEventListener("keyup", (e)=>{
            let code = e.which||e.keyCode;
            this.#pressed[code] = this.#pressedProp[code].timestamp = 0;
            this.#pressedProp[code].repeat    = false;
            this.#pressedProp[code].now       = new Date().getTime();
            e.preventDefault();
        });
        document.body.onblur = (e)=>{
            this.#pressed = new Uint8Array(0xff).fill(0x00);
            for( let tmp in this.#pressedProp ){
                this.#pressedProp[tmp].repeat   = false;
                this.#pressedProp[tmp].timestamp= 0;
            }
            e.preventDefault();
        };
    }

    /***
     * @param code
     * @param state
     */
    setEventKey( code = 0, state = null ){this.#pressed[code &= 0xff] = state!== null ? state : !this.#pressed[code] ? 1 : 0;}

    /***
     * @return {Uint8Array}
     */
    getEvent( ){return (new Uint8Array(this.#pressed));}

    /***
     * @param key
     * @return {*}
     */
    getKeyProperties( key = 0x00 ){return this.#pressedProp[key];}

    /***
     * @return {Uint8Array}
     */
    getKeysPressed( ){
        return new Uint8( this.getEvent().map((v,k)=>{
                return v === 1 ? k : false;
            }).filter(v=>v > 0)
        );
    }

    /***
     * @return {KeyboardEvents}
     */
    static getInstance( ){ return KeyboardEvents.INSTANCE; }

}

export default KeyboardEvents;