import KeyboardEvents from "./KeyboardEvents.js";

/***
 * Virtual key touch of spaceX displacement
 */
class Keypad {

    static INSTANCE = new Keypad();

    /***
     * Default keypad
     * @type {number}
     */
    static VK_UP    = KeyboardEvents.VK_UP_ARROW;
    static VK_LEFT  = KeyboardEvents.VK_LEFT_ARROW;
    static VK_RIGHT = KeyboardEvents.VK_RIGHT_ARROW;
    static VK_DOWN  = KeyboardEvents.VK_DOWN_ARROW;

    static VK_SHOOT         = KeyboardEvents.VK_SPACE;
    static VK_WEAPON_CHOICE = 0x43; // keypad "C"
    static VK_START         = KeyboardEvents.VK_ENTER;

    constructor() {
        if(Keypad.INSTANCE) return Keypad.INSTANCE;
    }

    /***
     * Reset keypad displacement
     * @return {Keypad}
     */
    reset( ){
        Keypad.VK_UP    = KeyboardEvents.VK_UP_ARROW;
        Keypad.VK_LEFT  = KeyboardEvents.VK_LEFT_ARROW;
        Keypad.VK_RIGHT = KeyboardEvents.VK_RIGHT_ARROW;
        Keypad.VK_DOWN  = KeyboardEvents.VK_DOWN_ARROW;
        return this;
    }
    /**
     * set virtual key touch
     *  0: ARROW_UP
     *  1: ARROW_LEFT
     *  2: ARROW_RIGHT
     *  3: ARROW_DOWN
     *  4: SHOOT
     *  5: WEAPON CHOICE
     * @param vk_type
     * @param vk_value
     * @return {Keypad}
     */
    setKeyPad( vk_type = 0xFF, vk_value = 0x00 ){
        vk_value &= 0xff;
        switch (vk_type) {
            case 0x00: Keypad.VK_UP    =vk_value; break;
            case 0x01: Keypad.VK_LEFT  =vk_value; break;
            case 0x02: Keypad.VK_RIGHT =vk_value; break;
            case 0x04: Keypad.VK_DOWN  =vk_value; break;
            case 0x08: Keypad.VK_SHOOT =vk_value; break;
            case 0x0A: Keypad.VK_WEAPON_CHOICE=vk_value; break;
            default: break;
        }
        return this;
    }

    static getInstance(){return Keypad.INSTANCE;}

}
export default Keypad;