class Render extends FrameBuffer{

    constructor( frame = null, dimension = null ) {
        super(frame,dimension||{});
    }

    drawSquareArea( animateObject = null ){
        if(animateObject){
            let vertex = animateObject.getVertexArea( true );
            super.bind( vertex[0].x, vertex[0].y, vertex[1].x, vertex[1].y, animateObject.getAreaColor() );
            super.bind( vertex[1].x, vertex[1].y, vertex[3].x, vertex[3].y, animateObject.getAreaColor() );
            super.bind( vertex[2].x, vertex[2].y, vertex[0].x, vertex[0].y, animateObject.getAreaColor() );
            super.bind( vertex[3].x, vertex[3].y, vertex[2].x, vertex[2].y, animateObject.getAreaColor() );
        }
        return this;
    }

    static builder( frame = null, dimension = null ){
        return new Render(frame,dimension);
    }

}

export default Render;